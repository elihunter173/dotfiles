" Currently (11-28-18), markdown syntax highlighting is bugged, so this
" disables it
let g:polyglot_disabled = ['markdown']
