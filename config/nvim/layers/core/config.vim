set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab " make <Tab> insert 4 spaces

set clipboard+=unnamedplus " make neovim use X clipboard

set hidden " make it possible to leave modified buffers
