" map Ctrl-o to opening denite to look at all files relative to current
" buffer's location

nmap <leader>s :Buffers<CR>
nmap <leader>o :Files<CR>
nmap <leader>T :Tags<CR>
nmap <leader>t :BTags<CR>
